#!/bin/bash
###################################################################################################
#
# Renewal gravity for pihole based on firebog and
# additional lists. The new created gravity
# will be exported and copied to another server and nextcloud.
#
# Syntax:       ./updateGravity.sh -o "full"
#               ./updateGravity.sh -o "checkAdditionalList"
#               ./updateGravity.sh -o "checkAdditionalListForDomain" -d "wetteronline" -r false
#               ./updateGravity.sh -o "exportGravityFromDB"
#               ./updateGravity.sh -o "updateNextcloud"
#
# Date:         2021-09-03
# Version:      1.1
#
# Changelog:    1.1 - Added function "checkAdditionalListForDomain" to search for a specified 
#                     domain in the addtional lists. Matching lists (URLs) can be removed.
#
###################################################################################################
# Preparations

fireBogUrl="https://v.firebog.net/hosts/lists.php?type=nocross"

piHoleDir="/etc/pihole"
nonCrsdList="${piHoleDir}/nonCrossed.list"
adList="${piHoleDir}/adlists.list"
gravityList="${piHoleDir}/gravity.list"
additionalList="${piHoleDir}/additional.list"
additionalDomains="${piHoleDir}/additionalDomains.list"
gravityDB="${piHoleDir}/gravity.db"

cleanAdlist=true

ncBaseUrl="https://"
ncUser=""
ncPasswdFile="/opt/pihole/ncPasswd"
ncFolder="Blacklists"

copyToExtServer=true
extServer="pi41"
extServerUser="mauri"

logFile="/var/log/updateGravity.log"
logContent=()

#################################################
# Functions

checkAdditionalList() {

    aUrlsNotReachable=()

    while IFS= read -r url
    do
        RESULT=`curl --silent --head "$url" | head -n 1`
        if ! echo "$RESULT" | grep "200"; then
            aNotReachable+="${url} "
        fi

    done < "$additionalList"

    if ! [ -z "$aNotReachable" ]; then

        for url in $aUrlsNotReachable
        do
            sed -i "\|${url}|d" $additionalList
            logContent+="Removed ${url}\n"
        done
    fi
}

#----------------------------

checkAdditionalListForDomain() {

    matches=()
    matchedUrls=()

    while IFS= read -r url
    do
        result=`curl --silent $url | grep "$domain"`

        if ! [ -z "$result" ]; then
            matches+="${result} "
            matchedUrls+="${url} "
        fi

    done < "$additionalList"

    echo "---------------------------"
    echo "Matches:"
    echo "---------------------------"

    for match in $matches
    do
        echo "$match"
    done

    echo "---------------------------"
    echo "URLs from additional list:"
    echo "---------------------------"

    for url in $matchedUrls
    do
        echo "$url"

        if $removeUrlsFromList; then
            sed -i "\|${url}|d" $additionalList
        fi
    done
}

#----------------------------

rebuildDB() {
   
    importSQL="/tmp/import.sql"

    echo "CREATE TEMP TABLE i(txt);" >> $importSQL
    echo ".separator ~" >> $importSQL
    echo ".import ${adList} i" >> $importSQL
    echo "INSERT OR IGNORE INTO adlist (address) SELECT txt FROM i;" >> $importSQL
    echo "DROP TABLE i;" >> $importSQL

    if $cleanAdlist; then

        flushSQL="/tmp/flush.sql"
        echo "DELETE FROM adlist;" >> $flushSQL
        sqlite3 $gravityDB < $flushSQL
        rm $flushSQL
    fi

    sqlite3 $gravityDB < $importSQL

    rm $importSQL
}

#----------------------------

exportGravityFromDB() {

    rm -f $gravityList

    exportSQL="/tmp/export.sql"

    echo ".output ${gravityList}" >> $exportSQL
    echo "select domain from gravity;" >> $exportSQL

    sqlite3 $gravityDB < $exportSQL

    rm $exportSQL
}

#----------------------------

updateNextcloud() {

    passwd=`cat $ncPasswdFile`

    result=`curl -u $ncUser:$passwd --silent --head "$ncBaseUrl/remote.php/webdav/$ncFolder/gravity.list" | head -n 1`

    if echo "$result" | grep "200"; then
        curl -X MOVE -u $ncUser:$passwd --header "Destination:$ncBaseUrl/remote.php/webdav/$ncFolder/gravity.list_$(date +%Y%m%d)" "$ncBaseUrl/remote.php/webdav/$ncFolder/gravity.list"
    fi

    curl -X PUT -u $ncUser:$passwd $ncBaseUrl/remote.php/webdav/$ncFolder/gravity.list --data-binary @"$gravityList"
}

#################################################
# Main

while getopts o:d:r: flag
do
    case "${flag}" in
        o) option=${OPTARG};;
        d) domain=${OPTARG};;
        r) removeUrlsFromList=${OPTARG};;
    esac
done

if [ -e $logFile ]; then

    logFileSize=$(wc -c <"$logFile")
    if [ $logFileSize -gt 5000000 ]; then
        mv $logFile "$logFile.$(date +%Y%m%d)"
        gzip "$logFile.$(date +%Y%m%d)"
    fi
fi

echo "***********************************" >> $logFile
echo `date` " - Starting..." >> $logFile

logContent+="Option: ${option}\n"

if [ "$option" = "full" ]; then

    if [ -e ${gravityList} ]; then
        domainsOld=`wc -l < $gravityList`
    else
        domainsOld=0
    fi

    # Cleanup

    logContent+="Cleanup...\n"

    rm -f $piHoleDir/list.*.*
    rm -f $adList
    rm -f $nonCrsdList

    # Download non crossed list from fireBog

    logContent+="Download list from fireBog...\n"

    curl $fireBogUrl > $nonCrsdList

    if ! [ -e $nonCrsdList ]; then
        logContent+="Error downloading list from fireBog!\n"
        printf $logContent >> $logFile
        exit 1
    fi

    # Check additional list

    logContent+="Checking additional lists...\n"

    checkAdditionalList

    # Build list

    sort $nonCrsdList $additionalList | uniq >> $adList

    # Build new gravity.db

    logContent+="Build new database...\n"

    rebuildDB

    # Do the update

    logContent+="Update gravity...\n"

    /usr/local/bin/pihole -g

    if [ $? -ne 0 ]; then
        logContent+="Error updating gravity.list!\n"
        printf $logContent >> $logFile
        exit 2
    fi

    logContent+="Export gravity from database to file...\n"

    # Export gravity to file

    exportGravityFromDB

    if ! [ -e ${gravityList} ]; then
        logContent+="Error updating gravity.list!\n"
        printf $logContent >> $logFile
        exit 3
    fi

    # Add some more exclusive domains to gravity.list

    logContent+="Adding custom URLs...\n"

    cat $additionalDomains >> $gravityList

    # Check old and new file

    domainsNew=`wc -l < $gravityList`
    
    logContent+="Number of Domains before update: ${domainsOld}\n"
    logContent+="Number of Domains after update: ${domainsNew}\n"

    size=$(wc -c <"$gravityList")

    if [ $size -lt 10000000 ]; then
        logContent+="Error! New gravity.list is smaller than 10MB!\n"
        printf $logContent >> $logFile
        exit 4
    fi

    # Copy to an external server if wanted (destination path is /tmp)
    # (This part is used for an external webserver which fetches the tar-file)

    if $copyToExtServer; then

        logContent+="Copy new gravity.list to external ${extServer}...\n"

        cp $gravityList /tmp/
        chown www-data:www-data /tmp/gravity.list
        tar -cf /tmp/gravityList.tar -C / tmp/gravity.list
        scp /tmp/gravityList.tar $extServerUser@$extServer:/tmp/
        rm -f /tmp/gravity.list
        rm -f /tmp/gravityList.tar
    fi

    # Nextcloud

    updateNextcloud

elif [ "$option" = "checkAdditionalList" ]; then

    checkAdditionalList

elif [ "$option" = "checkAdditionalListForDomain" ]; then

    checkAdditionalListForDomain

elif [ "$option" = "exportGravityFromDB" ]; then

    exportGravityFromDB

    if ! [ -e ${gravityList} ]; then
        logContent+="Error export gravity.list!\n"
        printf $logContent >> $logFile
        exit 1
    fi

    cat $additionalDomains >> $gravityList

elif [ "$option" = "updateNextcloud" ]; then

    updateNextcloud

else
    echo "Unknown Option (${option})!" >> $logFile
    exit 12
fi

#################################################
# End

printf "$logContent" >> $logFile

echo `date` " - SUCCESS." >> $logFile

exit 0